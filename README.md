# PROYECTO 2: ALIMENTACIÓN Y COVID

_Este proyecto consiste en la resolución de 4 hipótesis acerca de la alimentación en todos los países del mundo relacionados con el Covid. El Centro de Polı́tica y Promoción de la Nutrición del USDA recomienda una pauta de ingesta de dieta diaria muy simple: 30 % de granos, 40 % de verduras, 10 % de frutas y 20 % de proteı́na, pero ¿estamos realmente comiendo en el estilo de alimentación saludable recomendado por la USDA? ¿tiene el Covid alguna relación con respecto a una buena o mala alimentación?_

### Instalación:

1. Descargue el archivo proyecto.py y el archivo suministro_alimentos_kcal.py 
2. Abra su terminal y dirijase al lugar donde están ubicados los archivos
3. Escriba python3 proyecto.py en su terminal para ejecutar el programa

### Nota:

Algunos valores enteros del archivo suministro_alimentos_kcal.py fueron editados para transformarlos en porcentajes.

### Instrucciones:

1. El proyecto consiste en la resolución de las siguientes hipótesis:
- ¿Cual es el país con mayor consumo de alcohol?
- ¿Hay similitudes entre los mayores porcentajes de azucar y obesidad?
- ¿El seguimiento de la recomendación de la USDA en norteamérica es cumplida?
- ¿Existe relación entre el número de casos activos con las muertes por Covid?
2. Presione la tecla 1 y luego ENTER para resolver la hipótesis 1
3. Presione la tecla 2 y luego ENTER para resolver la hipótesis 2
4. Presione la tecla 3 y luego ENTER para resolver la hipótesis 3
5. Presione la tecla 4 y luego ENTER para resolver la hipótesis 4

### Creado con:

- GNU/Linux
- VIM
- Visual Studio Code
- Pandas
- Python3 
- GitLab

### Autores:

Patricia Fuentes - [PatriciaFuentesG](https://gitlab.com/PatriciaFuentesG)

Magdalena Lolas - [mlolas](https://gitlab.com/mlolas)

Paz Echeverría - [pazjecheverriaf](https://gitlab.com/pazjecheverriaf)