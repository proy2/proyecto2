import pandas as pan
import matplotlib.pyplot as plt
COMIDA = "suministro_alimentos_kcal.csv"
def abrir():
    archivo = pan.read_csv(COMIDA)
    return archivo
def exceso(data):
    data.drop(['Alcoholic Beverages', 'Animal Products', 'Animal fats',
                  'Aquatic Products, Other', 'Cereals - Excluding Beer',
                  'Eggs', 'Fish, Seafood', 'Fruits - Excluding Wine',
                  'Meat', 'Milk - Excluding Butter', 'Miscellaneous',
                  'Offals', 'Oilcrops', 'Pulses', 'Spices', 
                  'Starchy Roots', 'Stimulants', 'Sugar Crops', 
                  'Sugar & Sweeteners', 'Treenuts', 'Vegetal Products', 
                  'Vegetable Oils', 'Vegetables', 'Obesity', 
                  'Undernourished', 'Confirmed', 'Recovered'],
                  axis=1,
                  inplace=True)
    return data


def mayor_poblacion(data):
    mayor_p = data["Population"].max()
    print(f"el pais con la mayor poblacion es : {mayor_p}")
    mayor_decesos = data["Deaths"].max()
    print(f"el mayor numero de muertes es:{mayor_decesos}")


def menor_poblacion(data):
    menor_p = data["Population"].min()
    print(f"el pais con la menor poblacion es : {menor_p}")
    menor_decesos = data["Deaths"].min()
    print(f"el menor numero de muertes es:{menor_decesos}")


def graficos():
    columnas = data[(data ["Deaths"] == "mayor_decesos")
                        & (data ["Deaths"] == "menor_decesos")]

def hipotesis_relacion():
    data = abrir()
    archivo_editado = exceso(data)
    menor_p = menor_poblacion(data)
    mayor_p = mayor_poblacion(data)
    fig, ax2 = plt.subplots(nrows=2, ncols=1)
    data.plot(kind="hist", x="Population", ax=ax2[0])
    data.plot(kind="hist", x="Deaths", ax=ax2[1])
    plt.savefig("grafico_covid-19.png")
    print("con el grafico generado es posible establecer una relacion entre casos activos y muertes")
def eliminar_columnas(data):
    data.drop(["Alcoholic Beverages",
               "Animal fats",
               "Aquatic Products, Other",
               "Miscellaneous",
               "Offals",
               "Oilcrops",
               "Pulses",
               "Spices",
               "Starchy Roots",
               "Stimulants",
               "Sugar Crops",
               "Sugar & Sweeteners",
               "Treenuts",
               "Vegetable Oils",
               "Obesity",
               "Undernourished",
               "Confirmed",
               "Deaths",
               "Recovered",
               "Active",
               "Population",
               "Unit (all except Population)"],
               axis=1,
               inplace=True)
    data.drop(range(0, 26), axis=0, inplace=True)
    data.drop(range(27, 100), axis=0, inplace=True)
    data.drop(range(101, 161), axis=0, inplace=True)
    data.drop(range(162, 170), axis=0, inplace=True)
    return data

def crea_columnas_totales(data):
    data["Total Proteínas"] = data["Animal Products"] + data["Milk - Excluding Butter"] + data["Fish, Seafood"] + data["Meat"] + data["Eggs"]
    data["Total Vegetales"] = data["Vegetables"] + data["Vegetal Products"]
    data["Total Granos"] = data["Cereals - Excluding Beer"]
    data["Total Frutas"] = data["Fruits - Excluding Wine"]
    return data


def imprime_columnas(data):
    canada = data.loc[(data["Country"] == "Canada")]
    usa = data.loc[(data["Country"] == "United States of America")]
    mexico = data.loc[(data["Country"] == "Mexico")]
    print(canada)
    print("\n")
    print(usa)
    print("\n")
    print(mexico)
    return data

def resultados(data):
    print("La recomendación de la USDA para una dieta saludable es de 20% proteínas, 40% verduras, 30% granos y 10% frutas:")
    promedio_proteinas = data["Total Proteínas"].mean()
    print(f"\nEl promedio de la ingesta de proteínas en norteamerica es de {promedio_proteinas}%")
    promedio_vegetales = data["Total Vegetales"].mean()
    print(f"El promedio de la ingesta de vegetales en norteamerica es de {promedio_vegetales}%")
    promedio_granos = data["Total Granos"].mean()
    print(f"El promedio de la ingesta de granos en norteamerica es de {promedio_granos}%")
    promedio_frutas = data["Total Frutas"].mean()
    print(f"El promedio de la ingesta de frutas en norteamerica es de {promedio_frutas}%")
    print("\nSe puede concluir que en norteamerica no se cumple la recomendación de la USDA para una dieta saludable.")
    print("Deben aumentar la ingesta de frutas y granos considerablemente y mantener su ingesta de proteínas y verduras.")
    return data

def hipotesis_usda():
    data = abrir()
    new_data = eliminar_columnas(data)
    print("\n")
    crea_columnas_totales(data)
    imprime_columnas(data)
    print("\n")
    resultados(data)
def remover_exceso_alcohol(archivo):
    archivo.drop(['Animal Products',
                  'Obesity',
                  'Animal fats',
                  'Aquatic Products, Other',
                  'Cereals - Excluding Beer',
                  'Eggs',
                  'Fish, Seafood',
                  'Fruits - Excluding Wine',
                  'Meat',
                  'Milk - Excluding Butter', 
                  'Miscellaneous',
                  'Offals', 
                  'Oilcrops',
                  'Pulses', 
                  'Spices',
                  'Starchy Roots', 
                  'Stimulants',
                  'Treenuts', 
                  'Vegetal Products',
                  'Vegetable Oils',
                  'Vegetables',
                  'Undernourished', 
                  'Confirmed',
                  'Deaths',
                  'Recovered',
                  'Active',
                  'Sugar Crops',
                  'Sugar & Sweeteners',
                  'Population', 
                  'Unit (all except Population)'],
                   axis=1,
                   inplace=True)

    return archivo

def remover_exceso2(data):
    data.drop(['Alcoholic Beverages',
               'Animal Products', 
               'Animal fats',
               'Aquatic Products, Other', 
               'Cereals - Excluding Beer',
               'Eggs',
               'Fish, Seafood', 
               'Fruits - Excluding Wine', 
               'Meat',
               'Milk - Excluding Butter', 
               'Miscellaneous',
               'Offals', 
               'Oilcrops',
               'Pulses', 
               'Spices',
               'Starchy Roots', 
               'Stimulants',
               'Treenuts', 
               'Vegetal Products',
               'Vegetable Oils',
               'Vegetables',
               'Undernourished', 
               'Confirmed',
               'Deaths',
               'Recovered',
               'Active',
               'Sugar Crops',
               'Obesity',
               'Population',
               'Unit (all except Population)'],
                axis=1,
                inplace=True)
    return data

def remover_exceso1(archivo):
    archivo.drop(['Alcoholic Beverages',
                  'Animal Products', 
                  'Animal fats',
                  'Aquatic Products, Other', 
                  'Cereals - Excluding Beer',
                  'Eggs',
                  'Fish, Seafood', 
                  'Fruits - Excluding Wine', 
                  'Meat',
                  'Milk - Excluding Butter', 
                  'Miscellaneous',
                  'Offals', 
                  'Oilcrops',
                  'Pulses', 
                  'Spices',
                  'Starchy Roots', 
                  'Stimulants',
                  'Treenuts', 
                  'Vegetal Products',
                  'Vegetable Oils',
                  'Vegetables',
                  'Undernourished', 
                  'Confirmed',
                  'Deaths',
                  'Recovered',
                  'Active',
                  'Sugar Crops',
                  'Sugar & Sweeteners',
                  'Population', 
                  'Unit (all except Population)'],
                   axis=1,
                   inplace=True)
    return archivo

def mayor_obesidad(archivo):
    obesidad = 0
    mayor_o = 0
    max_o = None
    for num in range(170):
        obesidad = archivo["Obesity"][num].astype(type(float))
        if obesidad > mayor_o:
            mayor_o = obesidad
            max_o = archivo["Country"][num]
    obesidad = archivo.query("31.3 < Obesity <= 45.6")
    print(obesidad)
    return (obesidad , max_o)

def mayor_azucar(data):
    azucar = 0
    mayor_a = 0
    max_a = None
    for num in range(170):
        azucar = data["Sugar & Sweeteners"][num].astype(type(float))
        if azucar > mayor_a:
            mayor_a = azucar
            max_a = data["Country"][num]
    azucar = data[data['Sugar & Sweeteners'] >= 7.95]
    print(azucar)
    return (azucar , max_a)

def mayor_alcohol(data):
    alcohol = 0
    mayor_al = 0
    max_al = None
    for num in range(170):
        alcohol = data["Alcoholic Beverages"][num].astype(type(float))
        if alcohol > mayor_al:
            mayor_al = alcohol
            max_al = data["Country"][num]
    rango = data[data['Alcoholic Beverages'] >= 3]
    print(f"El pais con mayor porcentaje de consumo de alcohol es {max_al} con {mayor_al}")
    return rango
def imprimir_resultado_comparar(archivo, data):
    print("\nComparamos los paises mayores porcentajes de cada uno:",
          f"\nEl pais con mayor % de obesidad es {archivo} y % de azucar es {data}")
    
def grafico_alcohol(data):
    data.plot(kind="bar",x="Country")
    plt.savefig("Alcohol_mayor.png")

def grafico_comparar(archivo, data):
    fig, ax2 = plt.subplots(nrows=2, ncols=1)
    data.plot(kind="bar",x="Country", ax=ax2[0])
    archivo.plot(kind="bar",x="Country", ax=ax2[1])
    plt.savefig("grafico_comparacion.png")

def hipotesis_azucarvsobesidad():
    archivo = abrir()
    data = abrir()
    remover_exceso1(archivo)
    remover_exceso2(data)  
    (obesidad,max_o) = mayor_obesidad(archivo)
    (azucar,max_a) = mayor_azucar(data)
    imprimir_resultado_comparar(max_o, max_a)
    grafico_comparar(obesidad, azucar)
def hipotesis_alcohol():
    archivo = abrir()
    remover_exceso_alcohol(archivo)
    alcohol = mayor_alcohol(archivo)
    grafico_alcohol(alcohol)
def menu():
    print("------MENÚ--------",
          "\n1.-¿Cual es el país con mayor consumo de alcohol? ",
          "\n2.-¿Hay similitudes entre los mayores porcentajes de azúcar y obesidad?",
          "\n3.-¿El seguimiento de la recomendación de la USDA en norteamérica es cumplida?",
          "\n4.-¿Existe relación entre el número de casos activos con las muertes por Covid?")
    escoger =  int(input())
    if escoger == 1:
        hipotesis_alcohol()
    if escoger == 2:
        hipotesis_azucarvsobesidad()      
    if escoger == 3:
        hipotesis_usda()
    if escoger == 4:
        hipotesis_relacion()
def main():
    menu()
if __name__ == "__main__":                                                                                      
    main()                                                                                                                              


